import React from "react";
import AbourDescription from "../../components/pages/about";
import styles from "../../styles/Home.module.css";

const About = () => {
  return (
    <div className={styles.main}>
      <AbourDescription />
    </div>
  );
};

export default About;
