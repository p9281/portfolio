import { Grid } from "@mui/material";
import React from "react";
import Gradient from "rgt";

export default function AboutDescription() {
  return (
    <div
      style={{
        width: "50%",
        justifyContent: "center",
        textAlign: "center",
      }}
    >
      <img src="/Paul-no-bg.png" style={{ height: 100, width: 100 }} />
      <div>
        <Grid container>
          <Grid item>
            <p
              style={{
                textAlign: "justify",
                marginBottom: 20,
              }}
            >
              J’ai suivis une fomation d’ingénieur en informatique pendant 5 ans
              et je travaille en tant que développeur Fullstack js indépendament
              aujourd’hui. Je suis passionnées des technologies Javascript et
              côté backend, je travaille sur Nestjs et Expressjs tandisque côté
              front, j’utilise le plus souvent Reactjs avec la framework NEXTJS.
            </p>
          </Grid>
          <Grid item>
            <p
              style={{
                textAlign: "justify",
                marginBottom: 20,
              }}
            >
              J’ai suivis une fomation d’ingénieur en informatique pendant 5 ans
              et je travaille en tant que développeur Fullstack js indépendament
              aujourd’hui. Je suis passionnées des technologies Javascript et
              côté backend, je travaille sur Nestjs et Expressjs tandisque côté
              front, j’utilise le plus souvent Reactjs avec la framework NEXTJS.
            </p>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
